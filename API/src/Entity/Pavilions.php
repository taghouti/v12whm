<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PavilionsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=PavilionsRepository::class)
 */
class Pavilions
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=StorageAreas::class, mappedBy="pavilion")
     */
    private $storageAreas;

    public function __construct()
    {
        $this->storageAreas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|StorageAreas[]
     */
    public function getStorageAreas(): Collection
    {
        return $this->storageAreas;
    }

    public function addStorageArea(StorageAreas $storageArea): self
    {
        if (!$this->storageAreas->contains($storageArea)) {
            $this->storageAreas[] = $storageArea;
            $storageArea->setPavilion($this);
        }

        return $this;
    }

    public function removeStorageArea(StorageAreas $storageArea): self
    {
        if ($this->storageAreas->contains($storageArea)) {
            $this->storageAreas->removeElement($storageArea);
            // set the owning side to null (unless already changed)
            if ($storageArea->getPavilion() === $this) {
                $storageArea->setPavilion(null);
            }
        }

        return $this;
    }

    function __toString()
    {
        return $this->getName();
    }
}
