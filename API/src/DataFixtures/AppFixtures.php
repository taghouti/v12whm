<?php

namespace App\DataFixtures;

use App\Entity\Banner;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('admin@v12whm.esprit');
        $user->setUsername('admin');
        $user->setName('Admin');
        $user->setDob(new \DateTime('now'));
        $user->setGender('Male');
        $user->setPhone('0021621572886');
        $user->setPhoto('');
        $user->setPassword('$argon2id$v=19$m=65536,t=4,p=1$ioHs7yrqpixC9IpAp9ADKg$RaahObVfO/9G0ZXQk5Vd9ryonZi9COqQ0H2UIH7rmUI');
        $user->setRoles(['ROLE_ADMIN']);
        $user->setDisabled(0);
        $manager->persist($user);
        $manager->flush();
    }
}
