<?php

namespace App\Controller;

use App\Entity\Banner;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminController extends EasyAdminController
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * Create new element in the user entity with the hashed password
     * @param User $user
     */
    protected function persistUserEntity(User $user): void
    {
        $encodedPassword = $this->_encodePassword($user, $user->getPlainPassword());
        $user->setPassword($encodedPassword);
        parent::persistEntity($user);
    }

    /**
     * Return encoded password from the plain text one
     * @param User $user
     * @param String $password
     * @return string
     */
    private function _encodePassword(User $user, $password): string
    {
        return $this->encoder->encodePassword($user, $password);
    }

    /**
     * Update the user entity password
     * @param User $user
     */
    protected function updateUserEntity(User $user): void
    {
        $user->setPassword($user->getPassword());
        if (!empty($user->getPlainPassword())) {
            $encodedPassword = $this->_encodePassword($user, $user->getPlainPassword());
            $user->setPassword($encodedPassword);
        }
        parent::updateEntity($user);
    }
}
